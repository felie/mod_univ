#!/bin/bash
# transform parameters file if GET
if [ "$2" = 'GET' ]
then sed -i "s/\&/\n/g" $3
fi

# get the right config file (depend of suffix of $1)
extension="${1##*.}"
source "${0%/*}/config.$extension" # load configuration

if [ "$timer" = true ]
then start_time=$(($(date +%s%N)/1000000))
fi

boolean_value () {
    echo "$1 ="
    if [ "$2" = true ]
    then echo "true"
    else echo "false"
    fi
    echo "<br/>"
}

if [ "$debug" = true ]
then 
    echo "<span style='color:red'>config file: ${0%/*}/config.$extension</span><br/>"
    echo "<span style='color:red'>file: $1<br/>$2 data are in this file: $3</span><br/>"
fi

method=$2 # GET or POST

args=$3 # the name of a data file

#
# transformation of an alternation of html/ocam blocks into full ocaml code (html block are simply printed)
#
function prepare_code {
    # normalize <?ocaml and ?> position, alone and at the beginning of its line
    sed "s/<?$language/<?$language\n/g" $1 > $1.prep
    sed -i "s/?>/$separator\n?>\n/g" $1.prep
    # suppression de toutes les lignes vides
    sed -i '/^$/d' $1.prep
    # a priori out of ocaml code
    in=0
    while IFS= read -r line # read line (from $1.prep file)
    do
	if [[ $line = "<?$language" ]]
	then in=1 # we're entering the code
	else 
	    if [[ $line = "?>" ]]
	    then in=0 # we're getting out of the code
	    else
		if [[ $in = 0 ]] # for html code, simply print
		then
		    #line="$(echo $line | sed 's/\\/\\\\/g')"
		    #line="$(echo $line | sed 's/\"/\\\"/g')"
		    line="$(echo $line | sed 's|/|\\/|g')"
                    line="$(echo $line | sed 's|"|\\\\"|g')"
		    echo $print | sed "s/LINE/$line/"
		else 
		    echo "$line" # else, let it in ocaml
		fi
	    fi
	fi
    done < $1.prep # file to read
}

function info {
    # produce debug info
    if [ "$debug" = true ];
    then
       echo "<span style='color:red'>$1</span><br/>"
    fi
}

if [[ ! -f "$1" ]] 
then
    info "<b>$1</b> not found!"
else
    file="$1"
    file="${file%%.*}"
    if [ "$file.$suffix_i" -ot "$file.$language" ]; # if prepared code older than source
    then
       	rm -f "$file.$suffix_i"
	echo $prefix > "$file.$suffix_i" 2>&1
	echo $begin >> "$file.$suffix_i" 2>&1
	prepare_code "$file.$language" >> "$file.$suffix_i" 2>&1
	echo $end >> "$file.$suffix_i" 2>&1
        info "prepare code for interpreter"
    fi
    if [ "$compile" = true ];
    then
       if [ "$file.$suffix_c" -ot "$file.$suffix_i" ] # if compiled code older than source
       then
	   info "compiling the code"
	   $compiler "$file.$suffix_i" -o "$file.$suffix_c" 2>&1 # compilation of file
       fi
       $file.$suffix_c $method $args 2>&1 # exécution of file
       info "<b>using compiled version</b>"
    else
        $interpreter "$file.$suffix_i"
        info "<b>using interpreted version</b>"
    fi	   
fi

if [ "$timer" = true ]
then
    end_time=$(($(date +%s%N)/1000000))
    time_cost=$(($end_time - $start_time))
    echo "<br/><span style='color:green'><b>time cost = $time_cost ms</b></span><br/>"
fi

# show configuration file
if [ "$debug" = true ];
then
    echo 
    echo "<h4>Configuration file</h4>"
    echo "<pre><span style='color:red'>"
    cat "${0%/*}/config.$language"
    echo "</span></pre>"
fi

# show GET or POST parameters
if [ "$show_parameters" = true ];
then
    echo 
    echo "<h4>$2 parameters</h4>"
    echo "<pre><span style='color:red'>"
    cat $3   
    echo "</pre></span>"
fi

# cleaning GET or POST parameters
if [ "$debug" = false ];
then rm -f $3
fi

# show code
if [ "$showcode" = true ];
then 
    echo "<h4>Source code</h4>"
    echo "<pre><span style='color:blue'>"
    cat $1 | sed 's/</\&lt;/g' | sed 's/>/\&gt;/g'
    echo "</pre>"
fi
