
mod_univ is an apache2 module that allows any language to play the role of hml preprocessor, like php.

This module allows you to build a module for any language, even a compiled one.

In fact mod_univ is not a *true* apache module but a prototype to build apache2 modules connected with a unique bash preprocessor. But the performance is still correct: the compiled ocaml is as rapid as php for very simple page.  For complex calculation it will be better. 

# In few words

For each language, there is a configuration file, with some indications. You can produce your own for your favorite language.

## The script *prepare* does almost everything for you

1) produce the module C code 

2) compiling of the module with apxs[^1]

3) create a /opt/mod_univ directory

4) put in it
- preproc.sh file (the preproc.sh is by default universal one[^2])
- config.<language>

Be careful: always read a script before execute it with *sudo*!
```
sudo ./prepare <language>
```

At the end, the script displays the lines you need to add to /etc/apache2/apache2.conf

Do it, and reload apache2 (by *sudo service apache reload*). That's all

# Explanation of the principle

Algorithm of the system (module+preproc)[^3]
```
What the module do ?
     The module received (given by apache2) pages named *.<language>
     It produces a file with data (GET or POST)
     It runs preproc.sh with three parameters:
     	- the page name
	- the mode (GET or POST)
	- the name of GET or POST data file
  What the preproc.sh do ?
     If foo.language newer than foo.<suffix_i>
     	produces <foo.suffix_i> (print the html code and interpolate language blocks) 
     If compile option is true
        If <foo.language> newer than <foo.suffix_c>
	   produces foo.<suffix_c> with foo.<suffix_i> via the compiler
        execute foo.<suffix_c> $2 $3
     else
        execute interpreter <foo.suffix_i> $2 $3
The module read the standard output and return it to the browser
```

# options

There are options defined in the config file:

- debug (to give you some more text, and display config file)
- showcode (to show you the source code - not html :) of the page)
- timer (to tell you the execution time of preproc.sh)
- compile (for compiling or no)

# Demonstration

You can see mod_univ active for
- ocaml https://elie.org/mod_univ/demo.ocaml and https://elie.org/mod_univ/sort.ocaml
- python https://elie.org/mod_univ/demo.python
- C  https://elie.org/mod_univ/demo.codec

# Todo

- Process GET or POST data for different languages
- Run compiles file directly from module if exists

# Notes

[^1]: If you want a preproc only for your language, it's easy: change the *preproc.sh* to another file to call in the C code module before compiling it.
[^2]: mod_univ produced UTF8 documents.
[^3]: apxs is part of the apache2-dev package. To install it: *sudo apt install apache2-dev*

