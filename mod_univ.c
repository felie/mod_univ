/* 
 *  mod_univ.c 
 *  © François Elie & Marin Elie 2020
 *
 *  see LICENSE (GPL)
 *
 * to produce a module for a new language, you have to write a config_yourlanguage and produce the c code with produce_module.sh
 * 
 *  see README.md
 * 
 *  You can request *.language files and watch for the result
 */

#include "stdio.h"
#include "string.h"
#include "libgen.h"
#include "stdlib.h"
#include "httpd.h"
#include "http_config.h"
#include "http_protocol.h"
#include "apr_strings.h"

// to avoid using strncpy directly
char *subString(char *someString, int n) 
{
   char *new = malloc(sizeof(char)*n+1);
   strncpy(new, someString, n);
   new[n] = '\0';
   return new;
}

// return a random number
int random_number (int range) {
  srand ((unsigned int)time((time_t *) NULL));
  return (int)((double)rand() / ((double)RAND_MAX + 1) * range);
}

typedef struct {
  const char* key;
  const char* value;
} keyValuePair;

// for POST - produce a file of data (each line: variable=value)
write_post(request_rec* r,FILE *f) {
  apr_array_header_t *pairs = NULL;
  apr_off_t len;
  apr_size_t size;
  int res;
  int i = 0;
  char *buffer;
  keyValuePair* kvp;
  res = ap_parse_form_data(r, NULL, &pairs, -1, HUGE_STRING_LEN);
  if (res != OK || !pairs) return NULL;
  kvp = apr_pcalloc(r->pool, sizeof(keyValuePair) * (pairs->nelts + 1));
  while (pairs && !apr_is_empty_array(pairs)) {
    ap_form_pair_t *pair = (ap_form_pair_t *) apr_array_pop(pairs);
    apr_brigade_length(pair->value, 1, &len);
    size = (apr_size_t) len;
    buffer = apr_palloc(r->pool, size + 1);
    apr_brigade_flatten(pair->value, buffer, &size);
    buffer[len] = 0;
    kvp[i].key=apr_pstrdup(r->pool,pair->name);
    kvp[i].value=buffer;
    i++;
  }
  int n=0;
  for (n=i-1;n>=0;n--)
    fprintf(f,"%s = %s\n",kvp[n].key,kvp[n].value); // write in the file
}

static int MODULE_NAME_handler(request_rec *req)
{
  // if it is not a ocaml handler, declined
  if (strcmp(req->handler, "MODULE_NAME"))
    {
      return DECLINED;
    }
  
  FILE *fd;
  char command[255]="/opt/mod_univ/preproc.sh "; // the bash script command
  strcat(command,req->filename);
  char *sep;
  sep = strchr(command, ';'); // cut if there are ; avoid exécution of several commands! security
  if (sep != NULL) {
    *sep = '\0';
  }
  strcat(command," ");
  strcat(command,req->method);
  strcat(command," ");
  // production of the POST data file
  char post_file[255];
  char *dir=dirname(req->filename);
  sprintf(post_file,"%s/%d-%d",dir,time(NULL),random_number(1000000));
  //ap_rprintf(req,"fichier post %s<br/>",post_file);
  FILE *fp;
  fp=fopen(post_file,"w+"); // create the file to put GET/POST data
  if (strcmp(req->method,"GET") == 0)
    {
      if (req->args)
	{
	  //ap_rprintf(req,"les arguments: %s<br/>",req->args);
	  fprintf(fp,"%s",req->args);
	}
    }
  else
    {
      write_post(req,fp);  
    }
  fclose(fp);
  strcat(command,post_file);
  strcat(command," 2>&1");
      
  //command=/opt/mod_univ/preproc_MODULE_NAME.sh <page.ocaml> <method (GET or POST)> <name of the file containing data>

  fd = popen(command, "r"); // exécution of the script
 
  if (!fd) return 1;
  
  char   buffer[256];
  size_t chread;
  // String to store entire command contents in 
  size_t comalloc = 256;
  size_t comlen   = 0;
  char  *comout   = malloc(comalloc);
  
  while ((chread = fread(buffer, 1, sizeof(buffer), fd)) != 0) {
    if (comlen + chread >= comalloc) {
      comalloc *= 2;
           comout = realloc(comout, comalloc);
    }
    memmove(comout + comlen, buffer, chread);
    comlen += chread;
  }
    
    if (!req->header_only) { // data to send to browser
      ap_rputs(comout,req);
      free(comout);
      pclose(fd);
   } 

  req->content_type = "text/html;charset=UTF-8"; // tell to browser the type of document
  return OK;
}

static void MODULE_NAME_register_hooks(apr_pool_t *p)
{
  printf("\n ** MODULE_NAME_register_hooks  **\n\n");
  ap_hook_handler(MODULE_NAME_handler, NULL, NULL, APR_HOOK_MIDDLE);
}

// see https://httpd.apache.org/docs/2.4/developer/modguide.html

module AP_MODULE_DECLARE_DATA MODULE_NAME_module = {
  STANDARD20_MODULE_STUFF, 
  NULL, /* create per-dir    config structures */
  NULL, /* merge  per-dir    config structures */
  NULL, /* create per-server config structures */
  NULL, /* merge  per-server config structures */
  NULL, /* table of config file commands       */
  MODULE_NAME_register_hooks  /* register hooks */
};
